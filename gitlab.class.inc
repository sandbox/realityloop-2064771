<?php
/**
 * @file
 * Core Gitlab API method class.
 */

class gitlabMethod {

  /**
   * Protected variables.
   */
  protected $path = '/';
  protected $type = 'GET';
  protected $arguments = array();
  protected $return = FALSE;

  /**
   * Public variables.
   */
  public $response = array();
  public $settings = array();

  /**
   * Class contructor.
   */
  public function __construct($settings = array()) {
    if (empty($settings)) {
      $settings = variable_get('gitlab_settings', array());
    }
    $this->settings = $settings;
  }

  /**
   * Class getter.
   */
  public function __get($property) {
    if (property_exists($this, $property)) {
      return $this->$property;
    }
  }

  /**
   * Send callback; Makes the request and returns the data.
   */
  public function send($data = array()) {
    // Append 'private_token' to payload.
    $data['private_token'] = $this->settings['token'];

    // Build request URL.
    $url = $this->settings['url'] . $this->path;
    foreach ($data as $key => $value) {
      if ((is_string($value) || is_numeric($value)) && strstr($url, ":{$key}")) {
        $url = str_replace(":{$key}", $value, $url);
        unset($data[$key]);
      }
    }
    $url = url($url, array('query' => $data));

    // Process request.
    $this->response = drupal_http_request($url, array('method' => $this->type));
    if (in_array($this->response->code, array(200, 201)) && 'application/json' == $this->response->headers['content-type']) {
      $data = json_decode($this->response->data);
      return $data;
    }

    watchdog('gitlab', '<pre>' . print_r($this, TRUE) . '</pre>');
    return FALSE;
  }

}
