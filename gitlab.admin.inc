<?php
/**
 * @file
 * Adminstration functions for Moodle web services module.
 */

/**
 * Settings form callback.
 */
function gitlab_form($form, $form_state) {
  $settings = variable_get('gitlab_settings', array());

  $form['gitlab_settings'] = array(
    '#title' => t('API settings'),
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#collapsible' => FALSE,
  );

  $form['gitlab_settings']['url'] = array(
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['url']) ? $settings['url'] : '',
    '#description' => t('Example: http://example.com/api/v3'),
    '#required' => TRUE,
  );

  $form['gitlab_settings']['token'] = array(
    '#title' => t('Private token'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['token']) ? $settings['token'] : NULL,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
