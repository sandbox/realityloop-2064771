<?php
/**
 * @file
 * Gitlab API method(s); /projects.
 */

/**
 * GET /projects
 */
class gitlabProjectsGET extends gitlabMethod {
  protected $path = '/projects';
  protected $return = array(
    'projects' => array(
      'label' => 'Projects',
      'type' => 'list<gitlab_project>',
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('List projects'),
      'description' => t('Get a list of projects owned by the authenticated user.'),
    );
  }
}

/**
 * POST /projects
 */
class gitlabProjectsPOST extends gitlabMethod {
  protected $path = '/projects';
  protected $type = 'POST';
  protected $return = array(
    'project' => array(
      'label' => 'Project',
      'type' => 'gitlab_project',
    ),
  );
  protected $arguments = array(
    'name' => array(
      'name' => 'Name',
      'description' => 'New project name',
      'required' => TRUE,
    ),
    'description' => array(
      'name' => 'Description',
      'description' => 'Short project description',
    ),
    'default_branch' => array(
      'name' => 'Default branch',
      'description' => '"master" by default',
    ),
    'issues_enabled' => array(
      'name' => 'Issues enabled',
      'type' => 'boolean',
    ),
    'wall_enabled' => array(
      'name' => 'Wall enabled',
      'type' => 'boolean',
    ),
    'merge_requests_enabled' => array(
      'name' => 'Merge requests enabled',
      'type' => 'boolean',
    ),
    'wiki_enabled' => array(
      'name' => 'Wiki enabled',
      'type' => 'boolean',
    ),
    'snippets_enabled' => array(
      'name' => 'Snippets enabled',
      'type' => 'boolean',
    ),
    'public' => array(
      'name' => 'Public',
      'type' => 'boolean',
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('Create project'),
      'description' => t('Creates new project owned by user.'),
    );
  }
}
