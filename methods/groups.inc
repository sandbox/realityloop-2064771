<?php
/**
 * @file
 * Gitlab API method(s); /groups.
 */

/**
 * GET /groups
 */
class gitlabGroupsGET extends gitlabMethod {
  protected $path = '/groups';
  protected $return = array(
    'projects' => array(
      'label' => 'Project groups',
      'type' => 'list<gitlab_project_group>',
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('List project groups'),
      'description' => t('Get a list of groups. (As user: my groups, as admin: all groups)'),
    );
  }
}

/**
 * POST /groups
 */
class gitlabGroupsPOST extends gitlabMethod {
  protected $path = '/groups';
  protected $type = 'POST';
  protected $return = array(
    'project' => array(
      'label' => 'Project group',
      'type' => 'gitlab_project_group',
    ),
  );
  protected $arguments = array(
    'name' => array(
      'name' => 'Name',
      'description' => 'The name of the group',
      'required' => TRUE,
    ),
    'path' => array(
      'name' => 'Path',
      'description' => 'The path of the group',
      'required' => TRUE,
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('New group'),
      'description' => t('Creates a new project group. Available only for admin.'),
    );
  }
}
