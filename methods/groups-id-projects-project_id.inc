<?php
/**
 * @file
 * Gitlab API method(s); /groups/:id/projects/:project_id/
 */

/**
 * POST /groups/:id/projects/:project_id
 */
class gitlabGroupsIDProjectsPROJECTIDPOST extends gitlabMethod {
  protected $path = '/groups/:id/projects/:project_id';
  protected $type = 'POST';
  protected $return = array(
    'project' => array(
      'type' => 'gitlab_project_group',
      'label' => 'Project group',
    ),
  );
  protected $arguments = array(
    'id' => array(
      'name' => 'ID',
      'description' => 'The ID of a group',
      'type' => 'decimal',
      'required' => TRUE,
    ),
    'project_id' => array(
      'name' => 'Project ID',
      'description' => 'The ID of a project',
      'type' => 'decimal',
      'required' => TRUE,
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('Transfer project to group'),
      'description' => t('Transfer a project to the Group namespace. Available only for admin'),
    );
  }
}
