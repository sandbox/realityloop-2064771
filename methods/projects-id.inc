<?php
/**
 * @file
 * Gitlab API method(s); /projects/:id.
 */

/**
 * GET /projects/:id
 */
class gitlabProjectsIDGET extends gitlabMethod {
  protected $path = '/projects/:id';
  protected $return = array(
    'project' => array(
      'label' => 'Project',
      'type' => 'gitlab_project',
    ),
  );
  protected $arguments = array(
    'id' => array(
      'name' => 'ID',
      'description' => 'The ID or Name of a project',
      'required' => TRUE,
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('Get single project'),
      'description' => t('Get a specific project, identified by project ID or NAME, which is owned by the authentication user. Currently namespaced projects cannot retrieved by name.'),
    );
  }

  public static function conditionInfo() {
    return array(
      'name' => t('Project exists'),
      'description' => t(''),
    );
  }

  public function condition($response, $data) {
    // Currently namespaced projects cannot retrieved by name.
    if (is_string($data['id']) && !isset($response['data'])) {
      $method = gitlab_methods_get_method('projects', 'GET');
      $response = gitlab_method_callback($method->path, $method->type, $data);
      foreach ($response as $project) {
        if ($project->path == $data['id']) {
          return TRUE;
        }
      }
      return FALSE;
    }
    return (boolean) $response['data'];
  }
}
