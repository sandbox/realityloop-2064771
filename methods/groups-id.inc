<?php
/**
 * @file
 * Gitlab API method(s); /groups/:id.
 */

/**
 * GET /groups/:id
 */
class gitlabGroupsIDGET extends gitlabMethod {
  protected $path = '/groups/:id';
  protected $return = array(
    'project' => array(
      'label' => 'Project group',
      'type' => 'gitlab_project_group',
    ),
  );
  protected $arguments = array(
    'id' => array(
      'name' => 'ID',
      'description' => 'The ID of a group',
      'type' => 'decimal',
      'required' => TRUE,
    ),
  );

  public static function getInfo() {
    return array(
      'name' => t('Details of a group'),
      'description' => t('Get all details of a group.'),
    );
  }

  // public static function conditionInfo() {
  //   return array(
  //     'name' => t('Project exists'),
  //     'description' => t(''),
  //   );
  // }

  // public function condition($response, $data) {
  //   return (boolean) $response;
  // }
}
