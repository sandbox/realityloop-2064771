<?php
/**
 * @file
 * Devel module integration.
 */

/**
 * Implements hook_gitlab_menu_alter() on behalf of devel.module.
 */
function devel_gitlab_menu_alter(&$items) {
  $items['admin/config/services/gitlab/testing'] = array(
    'title' => 'Testing',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gitlab_testing_form'),
    'access arguments' => array('administer gitlab'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'gitlab.admin.inc',
  );
}

/**
 * Testing form callback.
 */
function gitlab_testing_form($form, $form_state) {
  $form['methods'] = array(
    '#title' => t('Method'),
    '#type' => 'select',
    '#options' => array(),
    '#ajax' => array(
      'callback' => 'gitlab_testing_form_ajax',
      'wrapper' => 'method-ajax-wrapper',
    ),
  );
  $methods = gitlab_methods_get_all();
  foreach ($methods as $id => $types) {
    foreach ($types as $type => $method) {
      $info = $method->getInfo();

      $form['methods']['#options']["{$id}::{$type}"] = t('@name (@type @path)', array(
        '@name' => $info['name'],
        '@type' => $type,
        '@path' => $method->path,
      ));
    }
  }
  asort($form['methods']['#options']);
  $form['methods']['#default_value'] = isset($form_state['values']['methods'])
    ? $form_state['values']['methods']
    : key($form['methods']['#options']);

  // Method parameters.
  list($id, $type) = explode('::', $form['methods']['#default_value']);
  $method = $methods[$id][$type];

  $form['method'] = array(
    '#title' => 'Method parameters',
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#prefix' => '<div id="method-ajax-wrapper">',
    '#suffix' => '</div>',
  );
  $parameters = $method->arguments;
  if (count($parameters) > 0) {
    foreach ($parameters as $key => $parameter) {
      $parameter += array(
        'description' => '',
        'type' => 'text',
        'default value' => '',
        'required' => FALSE,
      );

      $form['method'][$key] = gitlab_parameter_to_fapi($parameter);
      $form['method'][$key]['#default_value'] = isset($form_state['values']['method'][$key])
        ? $form_state['values']['method'][$key]
        : $form['method'][$key]['#default_value'];
    }
  }
  else {
    $form['method']['#description'] = t('This method has no parameters.');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Test'),
  );

  return $form;
}

/**
 * Testing form ajax callback.
 */
function gitlab_testing_form_ajax($form, $form_state) {
  return $form['method'];
}

/**
 * Testing form submit callback.
 */
function gitlab_testing_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;

  list($id, $type) = explode('::', $form_state['values']['methods']);
  $method = gitlab_methods_get_method($id, $type);

  $parameters = $method->arguments;
  $data = array();
  if (!empty($parameters)) {
    foreach (array_keys($parameters) as $parameter) {
      if (isset($form_state['values']['method'][$parameter])) {
        $data[$parameter] = $form_state['values']['method'][$parameter];
        if ((!isset($parameters[$parameter]['required']) || $parameters[$parameter]['required'] == FALSE) && (!isset($parameters[$parameter]['type']) || 'boolean' != $parameters[$parameter]['type']) && empty($data[$parameter])) {
          unset($data[$parameter]);
        }
      }
    }
  }

  $response = gitlab_method_callback($method->path, $method->type, $data);
  dpm($response);
}

