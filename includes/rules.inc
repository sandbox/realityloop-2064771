<?php
/**
 * @file
 * Rules module integration.
 */

/**
 * Implements hook_rules_action_info().
 */
function gitlab_rules_action_info() {
  $actions = array();

  foreach (gitlab_methods_get_all() as $id => $types) {
    foreach ($types as $type => $method) {
      $info = $method->getInfo();

      $actions["{$id}::{$type}"] = array(
        'label' => t('@name (@type @path)', array(
          '@name' => $info['name'],
          '@type' => $type,
          '@path' => $method->path,
        )),
        'group' => t('Gitlab API'),
        'base' => 'gitlab_rules_action',
      );

      // Parameters.
      $parameters = $method->arguments;
      if (!empty($parameters)) {
        $actions["{$id}::{$type}"]['parameter'] = array();
        foreach ($parameters as $key => $parameter) {
          $parameter += array(
            'description' => '',
            'type' => 'text',
            'default value' => '',
            'required' => FALSE,
          );

          $actions["{$id}::{$type}"]['parameter'][$key] = array(
            'label' => $parameter['name'],
            'description' => $parameter['description'],
            'type' => $parameter['type'],
            'default value' => $parameter['default value'],
            'optional' => !$parameter['required'],
          );
        }
      }

      // Provides.
      if ($method->return) {
        $actions["{$id}::{$type}"]['provides'] = $method->return;
        if (!is_array($actions["{$id}::{$type}"]['provides'])) {
          $actions["{$id}::{$type}"]['provides'] = array(
            'response' => array(
              'label' => t('Response'),
              'type' => '*',
            ),
          );
        }
      }
    }
  }

  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function gitlab_rules_condition_info() {
  $conditions = array();

  foreach (gitlab_methods_get_all() as $id => $types) {
    foreach ($types as $type => $method) {
      if (method_exists($method, 'conditionInfo')) {
        $info = $method->conditionInfo('condition');

        $conditions["{$id}::{$type}"] = array(
          'label' => t('@name (@type @path)', array(
            '@name' => $info['name'],
            '@type' => $type,
            '@path' => $method->path,
          )),
          'group' => t('Gitlab API'),
          'base' => 'gitlab_rules_condition',
        );

        // Parameters.
        $parameters = $method->arguments;
        if (!empty($parameters)) {
          $conditions["{$id}::{$type}"]['parameter'] = array();
          foreach ($parameters as $key => $parameter) {
            $parameter += array(
              'description' => '',
              'type' => 'text',
              'default value' => '',
              'required' => FALSE,
            );

            $conditions["{$id}::{$type}"]['parameter'][$key] = array(
              'label' => $parameter['name'],
              'description' => $parameter['description'],
              'type' => $parameter['type'],
              'default value' => $parameter['default value'],
              'optional' => !$parameter['required'],
            );
          }
        }
      }
    }
  }

  return $conditions;
}

/**
 * Implements hook_rules_data_info().
 */
function gitlab_rules_data_info() {
  $data = array();

  $data['gitlab_project'] = array(
    'label' => t('Gitlab API project'),
    'wrap' => TRUE,
    'property info' => array(
      'id' => array(
        'type' => 'decimal',
        'label' => t('The ID of the project'),
      ),
      'description' => array(
        'type' => 'text',
        'label' => t('Project description'),
      ),
      'default_branch' => array(
        'type' => 'text',
        'label' => t('Default branch'),
      ),
      'public' => array(
        'type' => 'boolean',
        'label' => t('Public access'),
      ),
      'ssh_url_to_repo' => array(
        'type' => 'uri',
        'label' => t('SSH URL to repository'),
      ),
      'http_url_to_repo' => array(
        'type' => 'uri',
        'label' => t('HTTP URL to repository'),
      ),
      'web_url' => array(
        'type' => 'uri',
        'label' => t('Project URL'),
      ),
      // 'owner' => array(
      //   'type' => 'gitlab_user',
      //   'description' => t('Project owner'),
      // ),
      'name' => array(
        'type' => 'text',
        'label' => t('The project name'),
      ),
      'name_with_namespace' => array(
        'type' => 'text',
        'label' => t('The project name with namespace'),
      ),
      'path' => array(
        'type' => 'text',
        'label' => t('The project path/internal name'),
      ),
      'path_with_namespace' => array(
        'type' => 'text',
        'label' => t('The project path/internal name with namesapce'),
      ),
      'issues_enabled' => array(
        'type' => 'boolean',
        'label' => t('Issues enabled'),
      ),
      'merge_requests_enabled' => array(
        'type' => 'boolean',
        'label' => t('Merge requests enabled'),
      ),
      'wall_enabled' => array(
        'type' => 'boolean',
        'label' => t('Wall enabled'),
      ),
      'wiki_enabled' => array(
        'type' => 'boolean',
        'label' => t('Wiki enabled'),
      ),
      'snippets_enabled' => array(
        'type' => 'boolean',
        'label' => t('Snippets enabled'),
      ),
      'created_at' => array(
        'type' => 'date',
        'label' => t('Created timestamp'),
      ),
      'last_activity_at' => array(
        'type' => 'date',
        'label' => t('Last activity timestamp'),
      ),
      // 'namespace' => array(
      //   'type' => 'gitlab_namespace',
      //   'label' => t('Namespace'),
      // ),
      'greatest_access_level' => array(
        'type' => 'decimal',
        'label' => t('Greatest access level'),
      ),
    ),
  );

  $data['gitlab_project_group'] = array(
    'label' => t('Gitlab API project group'),
    'wrap' => TRUE,
    'property info' => array(
      'id' => array(
        'type' => 'decimal',
        'label' => t('The project group ID'),
      ),
      'name' => array(
        'type' => 'text',
        'label' => t('The project group name'),
      ),
      'path' => array(
        'type' => 'text',
        'label' => t('The project group path/internal name'),
      ),
      'owner_id' => array(
        'type' => 'decimal',
        'label' => t('The project group owner ID'),
      ),
      'projects' => array(
        'type' => 'list<gitlab_project>',
        'label' => t('Projects in the group'),
      ),
    ),
  );

  return $data;
}

/**
 * Callback for dynamically generated rules actions.
 */
function gitlab_rules_action() {
  list($method, $data, $settings) = _gitlab_rules_process(func_get_args());

  $response = gitlab_method_callback($method->path, $method->type, $data);
  if (!$response) {
    // @TODO - Throw error.
  }

  if ($response && $method->return) {
    if (is_array($method->return)) {
      $return = $method->return;
      return array(key($return) => $response);
    }
    return array($settings['response:var'] => $response);
  }
}

/**
 * Callback for dynamically generated rules conditions.
 */
function gitlab_rules_condition() {
  $arguments = func_get_args();
  list($method, $data, $settings) = _gitlab_rules_process($arguments);

  $arguments[count($arguments) - 4]['response:var'] = 'data';
  $response = call_user_func_array('gitlab_rules_action', $arguments);
  return $method->condition($response, $data);
}

/**
 *
 */
function _gitlab_rules_process($arguments) {
  $element = $arguments[count($arguments) - 2];

  list($id, $type) = explode('::', $element->getElementName());
  $method = gitlab_methods_get_method($id, $type);

  $parameters = $method->arguments;
  $data = array();
  if (!empty($parameters)) {
    foreach (array_keys($parameters) as $parameter) {
      $data[$parameter] = array_shift($arguments);
      if ((!isset($parameters[$parameter]['required']) || $parameters[$parameter]['required'] == FALSE) && (!isset($parameters[$parameter]['type']) || 'boolean' != $parameters[$parameter]['type']) && empty($data[$parameter])) {
        unset($data[$parameter]);
      }
    }
  }

  $settings = array_shift($arguments);

  return array($method, $data, $settings);
}
