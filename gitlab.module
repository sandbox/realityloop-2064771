<?php
/**
 * @file
 * Core functions for the Gitlab API module.
 */

/**
 * Include additional files.
 */
$dirname = dirname(__FILE__) . '/includes';
$includes = file_scan_directory($dirname, '/.inc$/');
foreach (module_list() as $module) {
  $file = "{$dirname}/{$module}.inc";
  if (isset($includes[$file])) {
    require_once $file;
  }
}

/**
 * Implements hook_permission().
 */
function gitlab_permission() {
  return array(
    'administer gitlab' => array(
      'title' => t('Administer Gitlab API module'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function gitlab_menu() {
  $items = array();

  $items['admin/config/services/gitlab'] = array(
    'title' => 'Gitlab API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('gitlab_form'),
    'access arguments' => array('administer gitlab'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'gitlab.admin.inc',
  );

  $items['admin/config/services/gitlab/settings'] = array(
    'title' => 'Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'gitlab.admin.inc',
  );

  drupal_alter('gitlab_menu', $items);

  return $items;
}

/**
 * Method callback function.
 */
function gitlab_method_callback($method, $type = 'GET', $data = array()) {
  $method = gitlab_methods_get_method($method, $type);
  if ($method) {
    $response = $method->send($data);
    return $response;
  }

  watchdog('gitlab', 'The method %method does not exist.', array('%method' => $method), WATCHDOG_ERROR);
  return FALSE;
}

/**
 *
 */
function gitlab_methods_get_method($method, $type = 'GET') {
  $method = ltrim($method, '/');
  $method = str_replace(array('/', ':'), array('-', ''), $method);

  $type = strtoupper($type);
  $file = drupal_get_path('module', 'gitlab') . "/methods/{$method}.inc";
  if (file_exists($file)) {
    require_once $file;
    $method = str_replace('-', '_', $method);
    $class = 'gitlab' . gitlab_underscores_to_camelcase($method, TRUE) . $type;

    if (class_exists($class)) {
      $method = new $class;
      return $method;
    }
  }
  return FALSE;
}

/**
 *
 */
function gitlab_methods_get_all() {
  $methods = &drupal_static(__FUNCTION__, array());

  if (empty($methods)) {
    $dirname = dirname(__FILE__) . '/methods';
    $includes = file_scan_directory($dirname, '/.inc$/');
    foreach ($includes as $method) {
      foreach (array('GET', 'POST', 'PUT') as $type) {
        if ($result = gitlab_methods_get_method($method->name, $type)) {
          $methods[$method->name][$type] = $result;
        }
      }
    }
  }

  return $methods;
}

/**
 * Transform underscored strings to camelcased strings.
 *
 * @param $string
 *   The underscored string to be transformed.
 * @param $ucfirst
 *   A boolean value signifying whether the first characted should be
 *   uppercased.
 *
 * @return
 *   The transformed string.
 */
function gitlab_underscores_to_camelcase($string, $ucfirst = FALSE) {
  $parts = explode('_', $string);
  $parts = $parts ? array_map('ucfirst', $parts) : array($string);
  $parts[0] = $ucfirst ? ucfirst($parts[0]) : lcfirst($parts[0]);
  return implode('', $parts);
}

/**
 * Transform Parameter syntax into FAPI syntax.
 */
function gitlab_parameter_to_fapi($parameter) {
  $fapi = array();

  $fapi['#title'] = $parameter['name'];

  switch ($parameter['type']) {
    case 'decimal':
      $fapi['#element_validate'] = array('element_validate_number');
      $fapi['#type'] = 'textfield';
      break;

    case 'text':
      $fapi['#type'] = 'textfield';
      break;
  }

  $fapi['#default_value'] = $parameter['default value'];
  $fapi['#required'] = $parameter['required'];

  return $fapi;
}
